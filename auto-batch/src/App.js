import { useState } from "react";
import { flushSync } from "react-dom";
import { unstable_batchedUpdates } from "react-dom";

function App() {
  const [counter, setCounter] = useState(0);
  const [isEven, setIsEven] = useState(true);

  function handleClick() {
    const newCounter = counter + 1;
    // flushSync(() => {
      setCounter(newCounter);
    // });
    // flushSync(() => {
      setIsEven(!(newCounter % 2));
    // });
    /**
     * These state changes will trigger re-render ONLY once
     * This functionality will stay consistent in Promises,
     * callbacks like setTimeout, setInterval etc.
     * no matter what the context is!
     *
     * This is exactly what `unstable_batchedUpdates` does!
     */
  }

  const output = { counter, isEven };

  console.log(output);

  return (
    <div className="App">
      {JSON.stringify(output, null, 2)}
      <button onClick={handleClick}>change</button>
    </div>
  );
}

export default App;
