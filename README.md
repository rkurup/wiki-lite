# This is a demo app to showcase Server Components [(RFC)](https://github.com/josephsavona/rfcs/blob/server-components/text/0000-server-components.md) 

Steps:
0. navigate to an example folder
1. exec `yarn install` or `npm install`
2. exec `yarn dev` or `npm run dev`

----------

NOTE: I've simply modified the actual project for demo purposes. So all the credits goes to https://reactjs.org/blog/2020/12/21/data-fetching-with-react-server-components.html
