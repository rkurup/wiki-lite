import { fetch } from 'react-fetch';

export default function Content({ selectedId }) {
  const res = fetch(`https://en.wikipedia.org/?curid=${selectedId}`);
  return <div dangerouslySetInnerHTML={{ __html: res.text() }} />;
}
