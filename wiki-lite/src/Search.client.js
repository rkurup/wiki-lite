import { unstable_useTransition, useEffect, useState } from 'react';
import { useDebounce } from 'use-debounce';
import { useSync } from './Sync.context.client';
import Spinner from './Spinner';

export default function Search() {
  const [state, setState] = useState('');
  const [startSearching, isSearching] = unstable_useTransition(false);
  const [, setSync] = useSync();
  const [text] = useDebounce(state, 500);

  useEffect(() => {
    startSearching(() => {
      setSync(loc => ({
        ...loc,
        searchText: text
      }));
    });
  }, [text]);

  return (
    <>
      <div className='search-container search-common'>
        <h1>Wiki (lite)</h1>
        <div className='search-common'>
          <input
            type='text'
            value={state}
            placeholder='search something'
            onChange={e => {
              const newText = e.target.value;
              setState(newText);
            }}
          />
          <Spinner active={isSearching} />
        </div>
      </div>
    </>
  );
}
