import { format } from 'date-fns';
import { fetch } from 'react-fetch';
import ResultItem from './Result-Item.client';

const searchCache = new Map();

export default function Results({ searchText }) {
  let res = searchCache.get(searchText);

  if (!res) {
    res =
      searchText &&
      fetch(
        `https://en.wikipedia.org/w/api.php?${new URLSearchParams({
          action: 'query',
          list: 'search',
          prop: 'info',
          inprop: 'url',
          format: 'json',
          origin: '*',
          srlimit: 5,
          srsearch: searchText
        }).toString()}`
      ).json();
    searchCache.set(searchText, res);
  }

  return res ? (
    <ul className='results'>
      {res?.query?.search
        .sort((a, b) => (a.timestamp > b.timestamp ? 1 : -1))
        .map(m => (
          <ResultItem
            key={m.pageid}
            title={m.title}
            pageid={m.pageid}
            snippet={m.snippet}
            formattedDate={format(new Date(m.timestamp), 'dd-MMM-yyyy HH:ss')}
          />
        ))}
    </ul>
  ) : null;
}
