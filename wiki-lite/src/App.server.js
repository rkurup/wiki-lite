import { Suspense } from 'react';
import Overlay from './Overlay.client';
import Results from './Result.server';
import Search from './Search.client';
import Wiki from './Wiki.server';

export default function App({ selectedId, searchText }) {
  return (
    <>
      <Suspense fallback={<h1>Suspense: Search Field</h1>}>
        <Search />
      </Suspense>

      <Suspense fallback={<h1>Suspense: Result List</h1>}>
        <Results {...{ searchText }} />
      </Suspense>

      <Overlay
        childrenServer={
          selectedId && (
            <Suspense fallback={<h2>Suspense: Details of {selectedId}</h2>}>
              <Wiki {...{ selectedId }} />
            </Suspense>
          )
        }
      />
    </>
  );
}
