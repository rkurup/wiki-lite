import { Suspense, useState } from 'react';
import { unstable_createRoot } from 'react-dom';
import { ErrorBoundary } from 'react-error-boundary';
import { useServerResponse } from './Cache.client';
import { SyncContext } from './Sync.context.client';

const root = unstable_createRoot(document.getElementById('root'));

root.render(
  <Suspense fallback={<>Starting....</>}>
    <ErrorBoundary FallbackComponent={Error}>
      <Content />
    </ErrorBoundary>
  </Suspense>
);

function Content() {
  const [sync, setSync] = useState({
    selectedId: null,
    searchText: ''
  });
  const response = useServerResponse(sync);
  return (
    <SyncContext.Provider value={[sync, setSync]}>
      {response.readRoot()}
    </SyncContext.Provider>
  );
}

function Error({ error }) {
  return (
    <div>
      <h1>Application Error</h1>
      <pre style={{ whiteSpace: 'pre-wrap' }}>{error.stack}</pre>
    </div>
  );
}
