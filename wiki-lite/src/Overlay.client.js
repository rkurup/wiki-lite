import { useEffect, useState } from 'react';

export default function Overlay({ childrenServer }) {
  const [children, setChildren] = useState(childrenServer);

  useEffect(() => {
    setChildren(childrenServer);
  }, [childrenServer]);

  return children ? (
    <div className='overlay'>
      <button className='close' onClick={() => setChildren(null)}>
        &#x2716;
      </button>
      <div className='content'>{children}</div>
    </div>
  ) : null;
}
