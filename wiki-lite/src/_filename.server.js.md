Server Components: As a general rule, Server Components run once per request on the server, so they don’t have state and can’t use features that only exist on the client. 

❌ May not use state, because they execute (conceptually) only once per request, on the server. So useState() and useReducer() are not supported.
❌ May not use rendering lifecycle (effects). So useEffect() and useLayoutEffect() are not supported.
❌ May not use browser-only APIs such as the DOM (unless you polyfill them on the server).
❌ May not use custom hooks that depend on state or effects, or utility functions that depend on browser-only APIs.
✅ May use server-only data sources such as databases, internal (micro)services, filesystems, etc.
✅ May render other Server Components, native elements (div, span, etc), or Client Components.

NOTE: Server Hooks/Utilities - We can also create custom hooks or utility libraries that are designed for the server. All of the rules for Server Components apply. For example, one use-case for server hooks is to provide helpers for accessing server-side data sources.
