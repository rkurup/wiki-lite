import { unstable_useTransition } from 'react';
import { useSync } from './Sync.context.client';

export default function ResultItem(m) {
  const [, setSync] = useSync();
  const [startTransition] = unstable_useTransition();
  return (
    <li
      onClick={() => {
        startTransition(() => {
          setSync(loc => ({
            ...loc,
            selectedId: m.pageid
          }));
        });
      }}
    >
      <h2>
        <u>{m.title}</u>
      </h2>
      <b>{m.formattedDate}</b>
      <div dangerouslySetInnerHTML={{ __html: `<i>${m.snippet}</i>` }} />
    </li>
  );
}
