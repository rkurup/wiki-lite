'use strict';

const register = require('react-server-dom-webpack/node-register');
register();
const babelRegister = require('@babel/register');

babelRegister({
  ignore: [/[\\\/](build|backend|node_modules)[\\\/]/],
  presets: [['react-app', { runtime: 'automatic' }]],
  plugins: ['@babel/transform-modules-commonjs']
});

const { writeFile } = require('fs').promises;
const express = require('express');
const compress = require('compression');
const { readFileSync } = require('fs');
const { pipeToNodeWritable } = require('react-server-dom-webpack/writer');
const path = require('path');
const React = require('react');
const ReactApp = require('../src/App.server').default;

const PORT = 4001;
const app = express();

app.use(compress());
app.use(express.json());

app.listen(PORT, () => {
  console.log(`Header (lite) listening at http://localhost:${PORT}`);
});

app.get(
  '/',
  withErrorHandling(async function(_req, res) {
    await waitForWebpack();
    const html = readFileSync(
      path.resolve(__dirname, '../build/index.html'),
      'utf8'
    );
    // Note: this is sending an empty HTML shell, like a client-side-only app.
    // However, the intended solution (which isn't built out yet) is to read
    // from the Server endpoint and turn its response into an HTML stream.
    res.send(html);
  })
);

app.get('/sync', function(req, res) {
  sendResponse(req, res);
});

const DB_PATH = path.resolve(__dirname);

app.post(
  '/save',
  withErrorHandling(async function(req, res) {
    const { body } = req;
    await writeFile(
      path.resolve(DB_PATH, '../db/data.json'),
      JSON.stringify(body, null, 2)
    );

    sendResponse(req, res);
  })
);

app.use(express.static('build'));
app.use(express.static('public'));

app.on('error', function(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }
  var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
});

function withErrorHandling(fn) {
  return async function(req, res, next) {
    try {
      return await fn(req, res);
    } catch (x) {
      next(x);
    }
  };
}

async function renderReactTree(res, props) {
  await waitForWebpack();
  const manifest = readFileSync(
    path.resolve(__dirname, '../build/react-client-manifest.json'),
    'utf8'
  );
  const moduleMap = JSON.parse(manifest);
  pipeToNodeWritable(React.createElement(ReactApp, props), res, moduleMap);
}

function sendResponse(req, res) {
  const sync = JSON.parse(req.query.content);
  res.set('X-Sync', JSON.stringify(sync));
  renderReactTree(res, sync);
}

async function waitForWebpack() {
  while (true) {
    try {
      readFileSync(path.resolve(__dirname, '../build/index.html'));
      return;
    } catch (err) {
      console.log(
        'Could not find webpack build output. Will retry in a second...'
      );
      await new Promise(resolve => setTimeout(resolve, 1000));
    }
  }
}
