import { useState, unstable_useTransition } from 'react';
import { useSync } from './Sync.context.client';
import { useRefresh } from './Cache.client';
import { createFromReadableStream } from 'react-server-dom-webpack';
import useMutation from './mutation.hook';

function Stringify(p) {
  try {
    return JSON.stringify(JSON.parse(p), null, 3);
  } catch (error) {
    return p;
  }
}

export default function Editor() {
  const refresh = useRefresh();
  const [loggedIn, setLoggedIn] = useState(false);
  const [staticMenu, setStaticMenu] = useState(
    JSON.stringify([
      ['Books', ['Buy', 'Sell']],
      ['Study', ['Textbook Solutions', 'Expert', 'Practice', 'Learn']]
    ])
  );
  const [personalizedMenu, setPersnalizedMenu] = useState(
    JSON.stringify([['My Courses', ['Course A', 'Course B']]])
  );
  const [startNavigating, isNavigating] = unstable_useTransition();
  const [, setSync] = useSync();

  const [isSaving, saveDetails] = useMutation({
    endpoint: `/save`,
    method: 'POST'
  });

  async function handleSave() {
    const payload = {
      loggedIn,
      staticMenu: JSON.parse(staticMenu),
      personalizedMenu: JSON.parse(personalizedMenu)
    };
    const response = await saveDetails(payload);
    const cacheKey = response.headers.get('X-Sync');
    const newSync = JSON.parse(cacheKey);
    const seededResponse = createFromReadableStream(response.body);
    startNavigating(() => {
      refresh(cacheKey, seededResponse);
      setSync(newSync);
    });
  }

  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        handleSave();
      }}
    >
      <div className='editor'>
        <label>
          Static Menu
          <textarea
            name='static'
            disabled={isNavigating || isSaving}
            value={Stringify(staticMenu)}
            onChange={e => setStaticMenu(e.target.value)}
          />
        </label>
        <label>
          Personalized Menu
          <textarea
            name='personal'
            disabled={isNavigating || isSaving}
            value={Stringify(personalizedMenu)}
            onChange={e => setPersnalizedMenu(e.target.value)}
          />
        </label>
        <div style={{ flexDirection: 'column' }}>
          <div>
            <b>Logged State</b>
            <label>
              Signed In
              <input
                name='loggedIn'
                type='radio'
                value={true}
                checked={loggedIn === true}
                disabled={isNavigating || isSaving}
                onChange={e => setLoggedIn(e.target.checked)}
              />
            </label>
            <label>
              Signed Out
              <input
                name='loggedIn'
                type='radio'
                value={false}
                disabled={isNavigating || isSaving}
                checked={loggedIn === false}
                onChange={e => setLoggedIn(!e.target.checked)}
              />
            </label>
          </div>
          <button type='submit' style={{ height: 50 }}>
            save
          </button>
        </div>
      </div>
    </form>
  );
}
