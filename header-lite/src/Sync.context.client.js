import { createContext, useContext } from 'react';

export const SyncContext = createContext();

export function useSync() {
  return useContext(SyncContext);
}
