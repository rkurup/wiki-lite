Components and hooks that work on both the server and the client. This allows logic to be shared across environments so long as the components meet all the constraints of both Server and Client Components. Therefore, shared components and hooks:

❌ May not use state.
❌ May not use rendering lifecycle hooks such as effects.
❌ May not use browser-only APIs.
❌ May not use custom hooks or utilities that depend on state, effects, or browser APIs.
❌ May not use server-side data sources.
❌ May not render Server Components or use server hooks.
✅ May be used on the server and client.
