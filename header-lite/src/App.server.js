import { Suspense } from 'react';
import Editor from './Editor.client';
import Header from './Header.server';

export default function App() {
  return (
    <>
      <Suspense fallback={<h1>Suspense for the Header</h1>}>
        <Header />
      </Suspense>

      <Editor />
    </>
  );
}
