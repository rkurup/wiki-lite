import { useState } from 'react';

export default function useMutation({ endpoint, method }) {
  const [isSaving, setIsSaving] = useState(false);
  const [didError, setDidError] = useState(false);
  const [error, setError] = useState(null);
  if (didError) {
    // Let the nearest error boundary handle errors while saving.
    throw error;
  }

  async function performMutation(payload) {
    setIsSaving(true);
    const strPayload = JSON.stringify(payload);
    try {
      const response = await fetch(
        `${endpoint}?content=${encodeURIComponent(strPayload)}`,
        {
          method,
          body: strPayload,
          headers: {
            'Content-Type': 'application/json'
          }
        }
      );
      if (!response.ok) {
        throw new Error(await response.text());
      }
      return response;
    } catch (e) {
      setDidError(true);
      setError(e);
    } finally {
      setIsSaving(false);
    }
  }

  return [isSaving, performMutation];
}
