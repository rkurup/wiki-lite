import { useState } from 'react';
import MenuItems from './Menu-Items.server';

export default function HeaderMenu({ data }) {
  const [expanded, setExpanded] = useState(
    data.reduce((a, c) => ({ ...a, [c[0]]: false }), {})
  );
  return (
    <>
      {data.map(m => {
        const menuName = m[0];
        return (
          <div
            className='menu'
            key={menuName}
            onClick={() => {
              setExpanded(s => ({ ...s, [menuName]: !s[menuName] }));
            }}
          >
            <span className='menu-title'>{menuName}</span>
            {expanded[menuName] && <MenuItems items={[...m.slice(1)[0]]} />}
          </div>
        );
      })}
    </>
  );
}
