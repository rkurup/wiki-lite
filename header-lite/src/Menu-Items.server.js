export default function MenuItems({ items }) {
  return (
    <ul className='menu'>
      {items.map(m => (
        <li key={m}>{m}</li>
      ))}
    </ul>
  );
}
