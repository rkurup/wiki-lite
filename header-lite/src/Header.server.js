import { readFile } from 'react-fs';
import path from 'path';
import HeaderMenu from './Header-Menu.client';
import Logo from './Logo';

export default function Header() {
  const res = readFile(path.resolve(__dirname, '../db/data.json'), 'utf8');
  const data = JSON.parse(res);
  return (
    <header>
      <Logo />
      <HeaderMenu data={data.staticMenu} />
      {data.loggedIn ? (
        <>
          <HeaderMenu data={data.personalizedMenu} />
          <span>My Profile</span>
        </>
      ) : (
        <>Sign In</>
      )}
    </header>
  );
}
