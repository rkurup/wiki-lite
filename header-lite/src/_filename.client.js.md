Client Components: These are standard React components, which we've always used. The main new rules to consider are what they can’t do with respect to Server Components

❌ May not import Server Components or call server hooks/utilities, because those only work on the server.
However, a Server Component may pass another Server Component as a child to a Client Component: <ClientTabBar><ServerTabContent /></ClientTabBar>. From the Client Component’s perspective, its child will be an already rendered tree, such as the ServerTabContent output. This means that Server and Client components can be nested and interleaved in the tree at any level.
❌ May not use server-only data sources.
✅ May use state.
✅ May use effects.
✅ May use browser-only APIs.
✅ May use custom hooks and utilities that use state, effects or browser-only APIs.
